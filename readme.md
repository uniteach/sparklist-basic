# Spark List

## Intro

This is a simple application skeleton for teaching web applications in Java. We use:
-	Thymeleaf, HTML,  CSS as view technologies
- 	H2 SQL as database
-	And SparkJava as web framework


A more complete example is published in another repository.

## Development

### Branching
- master: the main branch for the latest release
- develop: head for development
- fx-abcde: feture branches